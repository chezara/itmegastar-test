import VueRouter from 'vue-router'
import Departments from './pages/Departments'
import Employees from './pages/Employees'
import CreateDepartment from './pages/CreateDepartment'
import CreateEmployee from './pages/CreateEmployee'
import DepartmentPage from './pages/DepartmentPage'
import EmployeePage from './pages/EmployeePage'

export default new VueRouter({
  routes: [
    {
      path: '/departments',
      component: Departments,
    },
    {
      path: '/employees',
      component: Employees
    },
    {
      path: '/departments/create',
      component: CreateDepartment
    },
    {
      path: '/departments/:departmentId',
      component: DepartmentPage,
      name: 'departmentPage' //именованный роут, без имени можно было бы юзать path
    },
    {
      path: '/employees/create',
      component: CreateEmployee
    },
    {
      path: '/employees/:employeeId',
      component: EmployeePage,
      name: 'employeePage' //именованный роут, без имени можно было бы юзать path
    },
  ],
  mode: 'history'
})
